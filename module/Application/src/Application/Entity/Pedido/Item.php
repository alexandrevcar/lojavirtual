<?php

namespace Application\Entity\Pedido;

class Item
{

    protected $id;
    protected $pedidoId;
    protected $produtoId;
    protected $precoTotal;
    protected $quantidade;

    public function getId()
    {
        return $this->id;
    }

    public function getPedidoId()
    {
        return $this->pedidoId;
    }

    public function getProdutoId()
    {
        return $this->produtoId;
    }

    public function getPrecoTotal()
    {
        return $this->precoTotal;
    }

    public function getQuantidade()
    {
        return $this->quantidade;
    }

    public function setId($id)
    {
        $this->id = $id;
        return $this;
    }

    public function setPedidoId($pedidoId)
    {
        $this->pedidoId = $pedidoId;
        return $this;
    }

    public function setProdutoId($produtoId)
    {
        $this->produtoId = $produtoId;
        return $this;
    }

    public function setPrecoTotal($precoTotal)
    {
        $this->precoTotal = $precoTotal;
        return $this;
    }

    public function setQuantidade($quantidade)
    {
        $this->quantidade = $quantidade;
        return $this;
    }

}
