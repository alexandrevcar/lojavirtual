<?php

namespace Application\Entity;

class Endereco
{

    const ESTADOS = [
        'AC' => 'Acre',
        'AL' => 'Alagoas',
        'AM' => 'Amazonas',
        'AP' => 'Amapá',
        'BA' => 'Bahia',
        'CE' => 'Ceará',
        'DF' => 'Distrito Federal',
        'ES' => 'Espírito Santo',
        'GO' => 'Goiás',
        'MA' => 'Maranhão',
        'MG' => 'Minas Gerais',
        'MS' => 'Mato Grosso do Sul',
        'MT' => 'Mato Grosso',
        'PA' => 'Pará',
        'PB' => 'Paraíba',
        'PE' => 'Pernambuco',
        'PI' => 'Piauí',
        'PR' => 'Paraná',
        'RJ' => 'Rio de Janeiro',
        'RN' => 'Rio Grande do Norte',
        'RO' => 'Rondônia',
        'RR' => 'Roraima',
        'RS' => 'Rio Grande do Sul',
        'SC' => 'Santa Catarina',
        'SE' => 'Sergipe',
        'SP' => 'São Paulo',
        'TO' => 'Tocantins',
    ];

    /**
     * @var string 00000-000
     */
    protected $cep;

    /**
     * @var string
     */
    protected $endereco;

    /**
     * @var string
     */
    protected $numero;

    /**
     * @var string
     */
    protected $complemento;

    /**
     * @var string
     */
    protected $bairro;

    /**
     * @var string
     */
    protected $cidade;

    /**
     * @var string
     */
    protected $uf;

    /**
     * @var string
     */
    protected $referencia;

    /**
     *
     * @return string
     */
    public function getCep()
    {
        return $this->cep;
    }

    /**
     * @return string
     */
    public function getEndereco()
    {
        return $this->endereco;
    }

    /**
     * @return string
     */
    public function getNumero()
    {
        return $this->numero;
    }

    /**
     * @return string
     */
    public function getComplemento()
    {
        return $this->complemento;
    }

    /**
     * @return string
     */
    public function getBairro()
    {
        return $this->bairro;
    }

    /**
     * @return string
     */
    public function getCidade()
    {
        return $this->cidade;
    }

    /**
     * @return string
     */
    public function getUf()
    {
        return $this->uf;
    }

    /**
     * @return string
     */
    public function getReferencia()
    {
        return $this->referencia;
    }

    /**
     * @param string $cep
     * @return \Application\Entity\Endereco
     */
    public function setCep(string $cep)
    {
        $this->cep = $cep;
        return $this;
    }

    /**
     * @param string $endereco
     * @return \Application\Entity\Endereco
     */
    public function setEndereco(string $endereco)
    {
        $this->endereco = $endereco;
        return $this;
    }

    /**
     * @param string $numero
     * @return \Application\Entity\Endereco
     */
    public function setNumero(string $numero)
    {
        $this->numero = $numero;
        return $this;
    }

    /**
     * @param string $complemento
     * @return \Application\Entity\Endereco
     */
    public function setComplemento(string $complemento)
    {
        $this->complemento = $complemento;
        return $this;
    }

    /**
     * @param string $bairro
     * @return \Application\Entity\Endereco
     */
    public function setBairro(string $bairro)
    {
        $this->bairro = $bairro;
        return $this;
    }

    /**
     * @param string $cidade
     * @return \Application\Entity\Endereco
     */
    public function setCidade(string $cidade)
    {
        $this->cidade = $cidade;
        return $this;
    }

    /**
     * @param string $uf
     * @return \Application\Entity\Endereco
     */
    public function setUf(string $uf)
    {
        $this->uf = $uf;
        return $this;
    }

    /**
     * @param string $referencia
     * @return \Application\Entity\Endereco
     */
    public function setReferencia(string $referencia)
    {
        $this->referencia = $referencia;
        return $this;
    }

}
