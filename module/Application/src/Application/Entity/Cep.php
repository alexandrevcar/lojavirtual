<?php

namespace Application\Entity;

use DateTime;

class Cep extends Endereco
{

    const LOGRADOUROS = [
        '1' => 'Alameda',
        '2' => 'Av.',
        '3' => 'Rua',
        '4' => 'Largo',
        '5' => 'Praça',
        '6' => 'Travessa',
        '7' => 'Rodovia',
        '8' => 'Quadra',
        '9' => 'Trevo',
        '10' => 'Via',
        '11' => 'Viela',
        '12' => 'Vila',
        '13' => 'Estrada',
        '14' => 'Outros',
        '15' => 'Viaduto',
    ];

    /**
     * @var int
     */
    protected $id;

    /**
     * @var DateTime
     */
    protected $dataCriacao;

    /**
     * @var DateTime
     */
    protected $dataAlteracao;

    /**
     * @var int
     */
    protected $logradouro;

    /**
     * @var string
     */
    protected $complemento2;

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return DateTime
     */
    public function getDataCriacao()
    {
        return $this->dataCriacao;
    }

    /**
     * @return DateTime
     */
    public function getDataAlteracao()
    {
        return $this->dataAlteracao;
    }

    /**
     * @return int
     */
    public function getLogradouro()
    {
        return $this->logradouro;
    }

    /**
     * @return string
     */
    public function getComplemento2()
    {
        return $this->complemento2;
    }

    /**
     * @param int $id
     * @return \Application\Entity\Cep
     */
    public function setId(int $id)
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @param DateTime $dataCriacao
     * @return \Application\Entity\Cep
     */
    public function setDataCriacao(DateTime $dataCriacao)
    {
        $this->dataCriacao = $dataCriacao;
        return $this;
    }

    /**
     * @param DateTime $dataAlteracao
     * @return \Application\Entity\Cep
     */
    public function setDataAlteracao(DateTime $dataAlteracao)
    {
        $this->dataAlteracao = $dataAlteracao;
        return $this;
    }

    /**
     * @param int $logradouro
     * @return \Application\Entity\Cep
     */
    public function setLogradouro(int $logradouro)
    {
        $this->logradouro = $logradouro;
        return $this;
    }

    /**
     * @param string $complemento2
     * @return \Application\Entity\Cep
     */
    public function setComplemento2(string $complemento2)
    {
        $this->complemento2 = $complemento2;
        return $this;
    }

}
