<?php

namespace Application\Entity;

use DateTime;

class Pedido
{

    protected $id;
    protected $data;
    protected $clienteId;
    protected $precoTotal;

    public function getId()
    {
        return $this->id;
    }

    public function getData()
    {
        return $this->data;
    }

    public function getClienteId()
    {
        return $this->clienteId;
    }

    public function getPrecoTotal()
    {
        return $this->precoTotal;
    }

    public function setId($id)
    {
        $this->id = $id;
        return $this;
    }

    public function setData(DateTime $data)
    {
        $this->data = $data;
        return $this;
    }

    public function setClienteId($clienteId)
    {
        $this->clienteId = $clienteId;
        return $this;
    }

    public function setPrecoTotal($precoTotal)
    {
        $this->precoTotal = $precoTotal;
        return $this;
    }

}
