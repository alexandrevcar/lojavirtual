<?php

namespace Application\Entity;

class Produto
{

    protected $id;
    protected $nome;
    protected $descricao;
    protected $imagem;
    protected $preco;

    public function getId()
    {
        return $this->id;
    }

    public function getNome()
    {
        return $this->nome;
    }

    public function getDescricao()
    {
        return $this->descricao;
    }

    public function getImagem()
    {
        return $this->imagem;
    }

    public function getPreco()
    {
        return $this->preco;
    }

    public function setId($id)
    {
        $this->id = $id;
        return $this;
    }

    public function setNome($nome)
    {
        $this->nome = $nome;
        return $this;
    }

    public function setDescricao($descricao)
    {
        $this->descricao = $descricao;
        return $this;
    }

    public function setImagem($imagem)
    {
        $this->imagem = $imagem;
        return $this;
    }

    public function setPreco($preco)
    {
        $this->preco = $preco;
        return $this;
    }

}
