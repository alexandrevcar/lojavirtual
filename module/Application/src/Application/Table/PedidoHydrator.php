<?php

namespace Application\Table;

use Zend\Stdlib\Hydrator\AbstractHydrator;
use Application\Entity\Pedido as PedidoEntity;
use DateTime;

/**
 * @Hydrator
 */
class PedidoHydrator extends AbstractHydrator
{

    /**
     * {@inheritDoc}
     * @throws \InvalidArgumentException
     */
    public function extract($object)
    {
        if (!($object instanceof PedidoEntity)) {
            throw new \InvalidArgumentException('The object does not match with the expected');
        }

        $data = array(
            'ped_id' => $object->getId() ?: null,
            'ped_cliente_id' => $object->getClienteId() ?: null,
            'ped_data' => $object->getData() instanceof DateTime ? $object->getData()->format('Y-m-d H:i:s') : null,
            'ped_preco_total' => (float) $object->getPrecoTotal(),
        );

        return $data;
    }

    /**
     * {@inheritDoc}
     * @throws \InvalidArgumentException
     */
    public function hydrate(array $data, $object)
    {
        if (!($object instanceof PedidoEntity)) {
            throw new \InvalidArgumentException('The object does not match with the expected');
        }

        $object->setId(isset($data['ped_id']) ? $data['ped_id'] : null)
            ->setClienteId(isset($data['ped_cliente_id']) ? $data['ped_cliente_id'] : null)
            ->setPrecoTotal((float) $data['ped_preco_total'])
            ->setData(DateTime::createFromFormat('Y-m-d H:i:s', $data['ped_data']));

        return $object;
    }

}
