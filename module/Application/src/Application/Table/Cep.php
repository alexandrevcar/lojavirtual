<?php

namespace Application\Table;

use Zend\Db\TableGateway\AbstractTableGateway;
use Application\Service\TraitCacheable;
use Zend\Db\Adapter\AdapterInterface;
use Zend\Db\ResultSet\HydratingResultSet;
use Application\Entity\Cep as CepEntity;
use Zend\Db\Sql\Select;

/**
 * @Table
 */
class Cep extends AbstractTableGateway
{

    use TraitCacheable;

    protected $table = 'tck360_cep';

    /**
     * @param \Zend\Db\Adapter\AdapterInterface $adapter
     */
    public function __construct(AdapterInterface $adapter)
    {
        $this->adapter = $adapter;
        $this->resultSetPrototype = new HydratingResultSet(new CepHydrator(), new CepEntity());
        $this->initialize();
    }

    /**
     * @param CepEntity $entity
     * @return bool
     */
    public function insertEntity(CepEntity $entity)
    {
        $data = $this->getResultSetPrototype()->getHydrator()->extract($entity);
        unset($data['id']);

        $affectedRows = parent::insert($data);
        if ($affectedRows) {
            $entity->setId($this->getLastInsertValue());
        }

        return (bool) $affectedRows;
    }

    /**
     * @param CepEntity $entity
     * @return bool
     */
    public function updateEntity(CepEntity $entity)
    {
        $data = $this->getResultSetPrototype()->getHydrator()->extract($entity);
        unset($data['cli_idCliente']);
        $where = array('cli_idCliente' => $entity->getId());

        $affectedRows = parent::update($data, $where);
        return (bool) $affectedRows;
    }

    /**
     * @param int $id
     * @return CepEntity
     */
    public function findById($id)
    {
        $select = new Select($this->table);
        $select->where->equalTo('id', (int) $id);

        $resultSet = $this->selectWith($select);
        return $resultSet->current();
    }

    /**
     * @param string $cep
     * @return CepEntity
     */
    public function findByCep(string $cep)
    {
        $select = new Select($this->table);
        $select->where->equalTo('cep', $cep);

        $resultSet = $this->selectWith($select);
        return $resultSet->current();
    }

}
