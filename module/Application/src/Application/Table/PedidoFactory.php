<?php

namespace Application\Table;

use Zend\ServiceManager\FactoryInterface;
use Zend\ServiceManager\ServiceLocatorInterface;

/**
 * @Factory
 */
class PedidoFactory implements FactoryInterface
{

    /**
     * {@inheritDoc}
     * @return \Application\Table\Pedido
     */
    public function createService(ServiceLocatorInterface $serviceLocator)
    {
        $dbAdapter = $serviceLocator->get('dbAdapterMaster');
        $table = new Pedido($dbAdapter);
        return $table;
    }

}
