<?php

namespace Application\Table;

use Zend\Db\TableGateway\AbstractTableGateway;
use Application\Service\TraitCacheable;
use Zend\Db\Adapter\AdapterInterface;
use Zend\Db\ResultSet\HydratingResultSet;
use Zend\Db\Sql\Select;
use Zend\Db\Sql\Where as Where;
use Zend\Paginator\Paginator;
use Zend\Paginator\Adapter\DbSelect;

/**
 * @Table
 */
class Pedido extends AbstractTableGateway
{

    use TraitCacheable;

    protected $table = 'loja_pedidos';

    /**
     * @param \Zend\Db\Adapter\AdapterInterface $adapter
     */
    public function __construct(AdapterInterface $adapter)
    {
        $this->adapter = $adapter;
        $this->resultSetPrototype = new HydratingResultSet(new \Application\Table\PedidoHydrator(), new \Application\Entity\Pedido());
        $this->initialize();
    }

    /**
     * @param \Application\Entity\Pedido $entity
     * @return bool
     */
    public function insertEntity(\Application\Entity\Pedido $entity)
    {
        $data = $this->getResultSetPrototype()->getHydrator()->extract($entity);
        unset($data['ped_id']);

        $affectedRows = parent::insert($data);
        if ($affectedRows) {
            $entity->setId($this->getLastInsertValue());
        }

        return (bool) $affectedRows;
    }

    /**
     * @param \Application\Entity\Pedido $entity
     * @return bool
     */
    public function updateEntity(\Application\Entity\Pedido $entity)
    {
        $data = $this->getResultSetPrototype()->getHydrator()->extract($entity);
        $where = array('ped_id' => $entity->getId());

        $affectedRows = parent::update($data, $where);
        return (bool) $affectedRows;
    }

    /**
     * @param int $id
     * @return <\Application\Entity\Pedido>
     */
    public function findById($id)
    {
        $select = new Select($this->table);
        $select->where->equalTo('ped_id', (int) $id);

        $resultSet = $this->selectWith($select);
        return $resultSet->current();
    }

    /**
     * @param Where $where
     * @param string $order
     * @return \Zend\Paginator\Paginator <\Application\Entity\Pedido>
     */
    public function fetchAllPaginated(Where $where = null, $order = null)
    {
        $select = new Select($this->table);

        if (null != $where) {
            $select->where($where);
        }
        if (null != $order) {
            $select->order($order);
        }

        $paginatorAdapter = new DbSelect($select, $this->adapter, clone $this->getResultSetPrototype());
        $paginator = new Paginator($paginatorAdapter);

        return $paginator;
    }

}
