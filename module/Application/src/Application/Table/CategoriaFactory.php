<?php

namespace Application\Table;

use Zend\ServiceManager\FactoryInterface;
use Zend\ServiceManager\ServiceLocatorInterface;

/**
 * @Factory
 */
class CategoriaFactory implements FactoryInterface
{

    /**
     * {@inheritDoc}
     * @return \Application\Table\Categoria
     */
    public function createService(ServiceLocatorInterface $serviceLocator)
    {
        $dbAdapter = $serviceLocator->get('dbAdapterMaster');
        $table = new Categoria($dbAdapter);
        return $table;
    }

}
