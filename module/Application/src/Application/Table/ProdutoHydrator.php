<?php

namespace Application\Table;

use Zend\Stdlib\Hydrator\AbstractHydrator;
use Application\Entity\Produto as ProdutoEntity;

/**
 * @Hydrator
 */
class ProdutoHydrator extends AbstractHydrator
{

    /**
     * {@inheritDoc}
     * @throws \InvalidArgumentException
     */
    public function extract($object)
    {
        if (!($object instanceof ProdutoEntity)) {
            throw new \InvalidArgumentException('The object does not match with the expected');
        }

        $data = array(
            'prod_id' => $object->getId() ?: null,
            'prod_nome' => $object->getNome() ?: null,
            'prod_descricao' => $object->getDescricao() ?: null,
            'prod_imagem' => $object->getImagem() ?: null,
            'prod_preco' => (float) $object->getPreco()
        );

        return $data;
    }

    /**
     * {@inheritDoc}
     * @throws \InvalidArgumentException
     */
    public function hydrate(array $data, $object)
    {
        if (!($object instanceof ProdutoEntity)) {
            throw new \InvalidArgumentException('The object does not match with the expected');
        }

        $object->setId(isset($data['prod_id']) ? $data['prod_id'] : null)
            ->setNome(isset($data['prod_nome']) ? $data['prod_nome'] : null)
            ->setDescricao(isset($data['prod_descricao']) ? $data['prod_descricao'] : null)
            ->setImagem(isset($data['prod_imagem']) ? $data['prod_imagem'] : null)
            ->setPreco((float) $data['prod_preco']);

        return $object;
    }

}
