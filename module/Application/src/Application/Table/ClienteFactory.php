<?php

namespace Application\Table;

use Zend\ServiceManager\FactoryInterface;
use Zend\ServiceManager\ServiceLocatorInterface;

/**
 * @Factory
 */
class ClienteFactory implements FactoryInterface
{

    /**
     * {@inheritDoc}
     * @return \Application\Table\Cliente
     */
    public function createService(ServiceLocatorInterface $serviceLocator)
    {
        $dbAdapter = $serviceLocator->get('dbAdapterMaster');
        $table = new Cliente($dbAdapter);
        return $table;
    }

}
