<?php

namespace Application\Table;

use Zend\ServiceManager\FactoryInterface;
use Zend\ServiceManager\ServiceLocatorInterface;

/**
 * @Factory
 */
class CepFactory implements FactoryInterface
{

    /**
     * {@inheritDoc}
     * @return \Application\Table\Cep
     */
    public function createService(ServiceLocatorInterface $serviceLocator)
    {
        $dbAdapter = $serviceLocator->get('dbAdapterSlave');
        $table = new Cep($dbAdapter);

        $cacheStorage = $serviceLocator->get('CacheStorage');
        $table->setCacheStorage($cacheStorage);

        return $table;
    }

}
