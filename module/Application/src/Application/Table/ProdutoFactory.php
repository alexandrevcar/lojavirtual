<?php

namespace Application\Table;

use Zend\ServiceManager\FactoryInterface;
use Zend\ServiceManager\ServiceLocatorInterface;

/**
 * @Factory
 */
class ProdutoFactory implements FactoryInterface
{

    /**
     * {@inheritDoc}
     * @return \Application\Table\Produto
     */
    public function createService(ServiceLocatorInterface $serviceLocator)
    {
        $dbAdapter = $serviceLocator->get('dbAdapterMaster');
        $table = new Produto($dbAdapter);
        return $table;
    }

}
