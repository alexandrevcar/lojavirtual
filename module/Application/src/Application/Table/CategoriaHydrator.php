<?php

namespace Application\Table;

use Zend\Stdlib\Hydrator\AbstractHydrator;
use Application\Entity\Categoria as CategoriaEntity;

/**
 * @Hydrator
 */
class CategoriaHydrator extends AbstractHydrator
{

    /**
     * {@inheritDoc}
     * @throws \InvalidArgumentException
     */
    public function extract($object)
    {
        if (!($object instanceof CategoriaEntity)) {
            throw new \InvalidArgumentException('The object does not match with the expected');
        }

        $data = array(
            'cat_id' => $object->getId() ?: null,
            'cat_nome' => $object->getNome() ?: null,
        );

        return $data;
    }

    /**
     * {@inheritDoc}
     * @throws \InvalidArgumentException
     */
    public function hydrate(array $data, $object)
    {
        if (!($object instanceof CategoriaEntity)) {
            throw new \InvalidArgumentException('The object does not match with the expected');
        }

        $object->setId(isset($data['cat_id']) ? $data['cat_id'] : null)
            ->setNome(isset($data['cat_nome']) ? $data['cat_nome'] : null);

        return $object;
    }

}
