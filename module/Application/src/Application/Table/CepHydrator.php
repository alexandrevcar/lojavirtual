<?php

namespace Application\Table;

use Zend\Stdlib\Hydrator\AbstractHydrator;
use DateTime;
use Application\Entity\Cep as CepEntity;

/**
 * @Hydrator
 */
class CepHydrator extends AbstractHydrator
{

    /**
     * {@inheritDoc}
     * @throws \InvalidArgumentException
     */
    public function extract($object)
    {
        if (!($object instanceof CepEntity)) {
            throw new \InvalidArgumentException('The object does not match with the expected');
        }

        $data = array(
            'id' => $object->getId() ?: null,
            'datacriacao' => $object->getDataCriacao() instanceof DateTime ? $object->getDataCriacao()->format('Y-m-d H:i:s') : null,
            'dataalteracao' => $object->getDataAlteracao() instanceof DateTime ? $object->getDataAlteracao()->format('Y-m-d H:i:s') : null,
            'cep' => $object->getCep() ?: null,
            'logradouro' => $object->getLogradouro() ?: null,
            'endereco' => $object->getLogradouro() ?: null,
            'complemento1' => $object->getComplemento() ?: null,
            'complemento2' => $object->getComplemento2() ?: null,
            'bairro' => $object->getBairro() ?: null,
            'cidade' => $object->getCidade() ?: null,
            'estado' => $object->getUf() ?: null,
        );

        return $data;
    }

    /**
     * {@inheritDoc}
     * @throws \InvalidArgumentException
     */
    public function hydrate(array $data, $object)
    {
        if (!($object instanceof CepEntity)) {
            throw new \InvalidArgumentException('The object does not match with the expected');
        }

        $object->setId(isset($data['id']) ? $data['id'] : null)
            ->setDataCriacao(DateTime::createFromFormat('Y-m-d H:i:s', $data['datacriacao']))
            ->setDataAlteracao(DateTime::createFromFormat('Y-m-d H:i:s', $data['dataalteracao']))
            ->setCep(isset($data['cep']) ? $data['cep'] : null)
            ->setLogradouro(isset($data['logradouro']) ? $data['logradouro'] : null)
            ->setEndereco(isset($data['endereco']) ? $data['endereco'] : null)
            ->setComplemento(isset($data['complemento1']) ? $data['complemento1'] : null)
            ->setComplemento2(isset($data['complemento1']) ? $data['complemento2'] : null)
            ->setBairro(isset($data['bairro']) ? $data['bairro'] : null)
            ->setCidade(isset($data['cidade']) ? $data['cidade'] : null)
            ->setUf(isset($data['estado']) ? $data['estado'] : null);

        return $object;
    }

}
