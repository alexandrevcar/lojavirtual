<?php

namespace Application\Table;

use Zend\Db\TableGateway\AbstractTableGateway;
use Application\Service\TraitCacheable;
use Zend\Db\Adapter\AdapterInterface;
use Zend\Db\ResultSet\HydratingResultSet;
use Zend\Db\Sql\Select;
use Zend\Db\Sql\Where;
use Zend\Paginator\Paginator;
use Zend\Paginator\Adapter\DbSelect;

/**
 * @Table
 */
class Produto extends AbstractTableGateway
{

    use TraitCacheable;

    protected $table = 'loja_produtos';

    /**
     * @param \Zend\Db\Adapter\AdapterInterface $adapter
     */
    public function __construct(AdapterInterface $adapter)
    {
        $this->adapter = $adapter;
        $this->resultSetPrototype = new HydratingResultSet(new \Application\Table\ProdutoHydrator(), new \Application\Entity\Produto());
        $this->initialize();
    }

    /**
     * @param \Application\Entity\Produto $entity
     * @return bool
     */
    public function insertEntity(\Application\Entity\Produto $entity)
    {
        $data = $this->getResultSetPrototype()->getHydrator()->extract($entity);
        unset($data['prod_id']);

        $affectedRows = parent::insert($data);
        if ($affectedRows) {
            $entity->setId($this->getLastInsertValue());
        }

        return (bool) $affectedRows;
    }

    /**
     * @param \Application\Entity\Produto $entity
     * @return bool
     */
    public function updateEntity(\Application\Entity\Produto $entity)
    {
        $data = $this->getResultSetPrototype()->getHydrator()->extract($entity);
        $where = array('prod_id' => $entity->getId());

        $affectedRows = parent::update($data, $where);
        return (bool) $affectedRows;
    }

    /**
     * @param int $id
     * @return <\Application\Entity\Produto>
     */
    public function findById($id)
    {
        $select = new Select($this->table);
        $select->where->equalTo('prod_id', (int) $id);

        $resultSet = $this->selectWith($select);
        return $resultSet->current();
    }

    /**
     * @param Where $where
     * @param string $order
     * @return \Zend\Paginator\Paginator <\Application\Entity\Produto>
     */
    public function fetchAllPaginated(Where $where = null, $order = null)
    {
        $select = new Select($this->table);
        $select->join('loja_produtos_categorias', 'prod_cat_produto_id = prod_id', [], Select::JOIN_LEFT)
            ->group('prod_id');

        if (null != $where) {
            $select->where($where);
        }
        if (null != $order) {
            $select->order($order);
        }

        $paginatorAdapter = new DbSelect($select, $this->adapter, clone $this->getResultSetPrototype());
        $paginator = new Paginator($paginatorAdapter);

        return $paginator;
    }

}
