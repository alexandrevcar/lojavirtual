<?php

namespace Application\Table\Pedido;

use Zend\ServiceManager\FactoryInterface;
use Zend\ServiceManager\ServiceLocatorInterface;

/**
 * @Factory
 */
class ItemFactory implements FactoryInterface
{

    /**
     * {@inheritDoc}
     * @return \Application\Table\Pedido\Item
     */
    public function createService(ServiceLocatorInterface $serviceLocator)
    {
        $dbAdapter = $serviceLocator->get('dbAdapterMaster');
        $table = new Item($dbAdapter);
        return $table;
    }

}
