<?php

namespace Application\Table\Pedido;

use Zend\Stdlib\Hydrator\AbstractHydrator;
use Application\Entity\Pedido\Item as ItemEntity;

/**
 * @Hydrator
 */
class ItemHydrator extends AbstractHydrator
{

    /**
     * {@inheritDoc}
     * @throws \InvalidArgumentException
     */
    public function extract($object)
    {
        if (!($object instanceof ItemEntity)) {
            throw new \InvalidArgumentException('The object does not match with the expected');
        }

        $data = array(
            'ped_item_id' => $object->getId() ?: null,
            'ped_item_pedido_id' => $object->getPedidoId() ?: null,
            'ped_item_produto_id' => $object->getProdutoId() ?: null,
            'ped_item_preco_total' => (float) $object->getPrecoTotal(),
            'ped_item_quantidade' => (int) $object->getQuantidade(),
        );

        return $data;
    }

    /**
     * {@inheritDoc}
     * @throws \InvalidArgumentException
     */
    public function hydrate(array $data, $object)
    {
        if (!($object instanceof ItemEntity)) {
            throw new \InvalidArgumentException('The object does not match with the expected');
        }

        $object->setId(isset($data['ped_item_id']) ? $data['ped_item_id'] : null)
            ->setPedidoId(isset($data['ped_item_pedido_id']) ? $data['ped_item_pedido_id'] : null)
            ->setProdutoId(isset($data['ped_item_produto_id']) ? $data['ped_item_produto_id'] : null)
            ->setPrecoTotal((float) $data['ped_item_preco_total'])
            ->setQuantidade((int) $data['ped_item_quantidade']);

        return $object;
    }

}
