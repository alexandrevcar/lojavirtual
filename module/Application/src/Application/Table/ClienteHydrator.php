<?php

namespace Application\Table;

use Zend\Stdlib\Hydrator\AbstractHydrator;
use Application\Entity\Cliente as ClienteEntity;

/**
 * @Hydrator
 */
class ClienteHydrator extends AbstractHydrator
{

    /**
     * {@inheritDoc}
     * @throws \InvalidArgumentException
     */
    public function extract($object)
    {
        if (!($object instanceof ClienteEntity)) {
            throw new \InvalidArgumentException('The object does not match with the expected');
        }

        $data = array(
            'cli_id' => $object->getId() ?: null,
            'cli_nome' => $object->getNome() ?: null,
            'cli_email' => $object->getEmail() ?: null,
            'cli_senha' => $object->getSenha() ?: null,
        );

        return $data;
    }

    /**
     * {@inheritDoc}
     * @throws \InvalidArgumentException
     */
    public function hydrate(array $data, $object)
    {
        if (!($object instanceof ClienteEntity)) {
            throw new \InvalidArgumentException('The object does not match with the expected');
        }

        $object->setId(isset($data['cli_id']) ? $data['cli_id'] : null)
            ->setNome(isset($data['cli_nome']) ? $data['cli_nome'] : null)
            ->setEmail(isset($data['cli_email']) ? $data['cli_email'] : null)
            ->setSenha(isset($data['cli_senha']) ? $data['cli_senha'] : null);

        return $object;
    }

}
