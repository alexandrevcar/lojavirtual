<?php

namespace Application\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;

class Main extends AbstractActionController
{

    public function homeAction()
    {
        $viewModel = new ViewModel();
        $produtoTable = $this->getServiceLocator()->get('Application\Table\Produto');
        $produtoPaginator = $produtoTable->fetchAllPaginated(null, "prod_nome");
        $produtoPaginator->setCurrentPageNumber((int) $this->params()->fromQuery('p', 1));
        $produtoPaginator->setItemCountPerPage(9);

        $viewModel->setVariable('produtos', $produtoPaginator);

        return $viewModel;
    }

    public function searchAction()
    {
        $viewModel = new ViewModel();
        $viewModel->setTemplate('application/main/home');
        $produtoTable = $this->getServiceLocator()->get('Application\Table\Produto');

        $where = new \Zend\Db\Sql\Where;
        $where->like('prod_nome', "%{$this->params()->fromRoute('busca')}%");
        $where->like('prod_descricao', "%{$this->params()->fromQuery('busca')}%");

        $produtoPaginator = $produtoTable->fetchAllPaginated($where);
        $produtoPaginator->setCurrentPageNumber((int) $this->params()->fromQuery('p', 1));
        $produtoPaginator->setItemCountPerPage(9);

        $viewModel->setVariable('produtos', $produtoPaginator);
        return $viewModel;
    }

    public function categoryAction()
    {
        $viewModel = new ViewModel();
        $viewModel->setTemplate('application/main/home');
        $produtoTable = $this->getServiceLocator()->get('Application\Table\Produto');

        $where = new \Zend\Db\Sql\Where;
        $where->equalTo('prod_cat_categoria_id', $this->params()->fromRoute('id'));

        $produtoPaginator = $produtoTable->fetchAllPaginated($where);
        $produtoPaginator->setCurrentPageNumber((int) $this->params()->fromQuery('p', 1));
        $produtoPaginator->setItemCountPerPage(9);

        $viewModel->setVariable('produtos', $produtoPaginator);
        return $viewModel;
    }

}
