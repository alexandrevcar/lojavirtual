<?php

namespace Application\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;
use Application\Controller\Plugin\Messages;

class Login extends AbstractActionController
{

    public function indexAction()
    {
        $authService = $this->getServiceLocator()->get('AuthService');
        if ($authService->hasIdentity()) {
            return $this->redirect()->toRoute('site');
        } else {
            $viewModel = new ViewModel();
            if ($this->getRequest()->isPost()) {
                $authAdapter = $authService->getAdapter();
                $authAdapter->setIdentity($this->params()->fromPost('email'));
                $authAdapter->setCredential($this->params()->fromPost('senha'));

                if ($authService->authenticate()->isValid()) {
                    if ($this->params()->fromPost('redirect')) {
                        $url = "{$this->params()->fromPost('redirect')}";
                        return $this->redirect()->toUrl($url);
                    } else {
                        return $this->redirect()->toRoute('site');
                    }
                } else {
                    $this->Messages()->add('Não foi possível a autenticação. Por favor, verifique seu e-mail e senha.', Messages::TYPE_DANGER);
                }
            } else {
                $viewModel->setVariable('redirect', $this->params()->fromQuery('redirect'));
            }
            return $viewModel;
        }
    }

    public function logoutAction()
    {
        $authService = $this->getServiceLocator()->get('AuthService');

        if ($authService->hasIdentity()) {
            $authService->clearIdentity();
        }

        return $this->redirect()->toRoute('site');
    }

}
