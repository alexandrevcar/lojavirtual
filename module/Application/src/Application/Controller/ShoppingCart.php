<?php

namespace Application\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;
use Zend\View\Model\JsonModel;
use Zend\Session\Container;
use Application\Controller\Plugin\Messages;

class ShoppingCart extends AbstractActionController
{

    public function indexAction()
    {
        $viewModel = new ViewModel();
        $shoppingCart = new Container('shoppingcart');
        $viewModel->setVariable('shoppingcart', $shoppingCart);
        return $viewModel;
    }

    public function addAction()
    {
        $produtoTable = $this->getServiceLocator()->get('Application\Table\Produto');
        $produto = $produtoTable->findById($this->params()->fromRoute('id'));
        if ($produto) {
            $shoppingCart = new Container('shoppingcart');
            if (!$shoppingCart->items) {
                $shoppingCart->items = [];
            }
            $shoppingCart->items[$produto->getId()] = [
                'id' => $produto->getId(),
                'nome' => $produto->getNome(),
                'preco' => $produto->getPreco(),
                'total' => $produto->getPreco(),
                'qtde' => 1
            ];
        }
        $this->applyTotalValues();
        return $this->forward()->dispatch('Application\Controller\ShoppingCart', ['action' => 'index']);
    }

    public function removeAction()
    {
        $produtoTable = $this->getServiceLocator()->get('Application\Table\Produto');
        $shoppingCart = new Container('shoppingcart');
        $produto = $produtoTable->findById($this->params()->fromRoute('id'));
        if ($produto) {
            if (isset($shoppingCart->items[$produto->getId()])) {
                unset($shoppingCart->items[$produto->getId()]);
            }
            $this->applyTotalValues();
        }
        return $this->forward()->dispatch('Application\Controller\ShoppingCart', ['action' => 'index']);
    }

    public function updateAction()
    {
        if ($this->getRequest()->isPost()) {
            $shoppingCart = new Container('shoppingcart');
            $items = $this->params()->fromPost('items');
            if (!$shoppingCart->items) {
                $shoppingCart->items = [];
            }
            foreach ($items as $item => $qtde) {
                if ($qtde > 0) {
                    $preco = $shoppingCart->items[$item]['preco'];
                    $shoppingCart->items[$item]['total'] = $preco * $qtde;
                    $shoppingCart->items[$item]['qtde'] = $qtde;
                } else if (isset($shoppingCart->items[$item])) {
                    unset($shoppingCart->items[$item]);
                }
            }
            $this->applyTotalValues();

            if ($this->params()->fromPost('acao') == 'finalizar') {
                return $this->redirect()->toRoute('site/shopping-cart/confirm');
            }
        }
        return $this->forward()->dispatch('Application\Controller\ShoppingCart', ['action' => 'index']);
    }

    private function applyTotalValues()
    {
        $shoppingCart = new Container('shoppingcart');
        $total = $totalSub = $totalTaxa = 0;
        foreach ($shoppingCart->items as $item) {
            $total += $item['total'];
            $totalSub += $item['preco'] * $item['qtde'];
        }
        if (!count($shoppingCart->items)) {
            $shoppingCart->getManager()->getStorage()->clear('shoppingcart');
        } else {
            $shoppingCart['total'] = $total;
        }
    }

    public function confirmAction()
    {
        $viewModel = new ViewModel();
        $shoppingCart = new Container('shoppingcart');
        if ($this->params()->fromQuery('fim') == true) {
            try {
                $authService = $this->getServiceLocator()->get('AuthService');
                $pedidoTable = $this->getServiceLocator()->get('Application\Table\Pedido');
                $itemTable = $this->getServiceLocator()->get('Application\Table\Pedido\Item');

                // Adiciona o pedido
                $pedidoEntity = new \Application\Entity\Pedido();
                $pedidoEntity->setClienteId($authService->getIdentity()->getId())
                    ->setPrecoTotal($shoppingCart['total'])
                    ->setData(new \DateTime());
                $pedidoTable->insertEntity($pedidoEntity);

                // Adiciona os items
                foreach ($shoppingCart->items as $item) {
                    $itemEntity = new \Application\Entity\Pedido\Item();
                    $itemEntity->setProdutoId($item['id'])
                        ->setPrecoTotal($item['total'])
                        ->setQuantidade($item['qtde'])
                        ->setPedidoId($pedidoEntity->getId());
                    $itemTable->insertEntity($itemEntity);
                }
                $shoppingCart->getManager()->getStorage()->clear('shoppingcart');
                return $this->redirect()->toRoute('site/shopping-cart/orders');
            } catch (\Exception $ex) {
                $this->Messages()->add('Problema ao confirmar a compra', Messages::TYPE_DANGER);
            }
        }
        $viewModel->setVariable('shoppingcart', $shoppingCart);
        return $viewModel;
    }

    protected function bindTreeGrid($entity)
    {
        return [
            'nodeId' => "treegrid-{$entity->getId()}",
            'nodeParent' => null,
            'id' => $entity->getId(),
            'data' => $entity->getData()->format('d/m/Y'),
            'precoTotal' => $entity->getPrecoTotal(),
        ];
    }

    protected function bindTreeGridItem($entity)
    {
        return [
            'nodeId' => "treegrid-{$entity->getId()}",
            'nodeParent' => $entity->getPedidoId(),
            'id' => $entity->getId(),
            'produto' => $entity->getProdutoId(),
            'qtde' => $entity->getQuantidade(),
            'precoTotal' => $entity->getPrecoTotal(),
        ];
    }

    public function ordersAction()
    {
        $viewModel = new ViewModel();

        $authService = $this->getServiceLocator()->get('AuthService');
        $pedidoTable = $this->getServiceLocator()->get('Application\Table\Pedido');
        $itemTable = $this->getServiceLocator()->get('Application\Table\Pedido\Item');

        $where = new \Zend\Db\Sql\Where;
        $where->equalTo('ped_cliente_id', $authService->getIdentity()->getId());
        $pedidoPaginator = $pedidoTable->fetchAllPaginated($where, "ped_data DESC");
        $pedidoPaginator->setCurrentPageNumber((int) $this->params()->fromQuery('p', 1));
        $pedidoPaginator->setItemCountPerPage(10);

        $viewModel->setVariable('pedidoPaginator', $pedidoPaginator);
        return $viewModel;
    }

    public function ordersDetailAction()
    {
        $viewModel = new ViewModel();
        $pedidoTable = $this->getServiceLocator()->get('Application\Table\Pedido');
        $itemTable = $this->getServiceLocator()->get('Application\Table\Pedido\Item');
        $produtoTable = $this->getServiceLocator()->get('Application\Table\Produto');
        $pedido = $pedidoTable->findById($this->params()->fromRoute('pedido'));

        $where = new \Zend\Db\Sql\Where;
        $where->equalTo('ped_item_pedido_id', $pedido->getId());
        $itemList = $itemTable->fetchAllPaginated($where);
        foreach ($itemList as $item) {
            $item->produto = $produtoTable->findById($item->getProdutoid());
        }

        $viewModel->setVariable('pedido', $pedido);
        $viewModel->setVariable('itemList', $itemList);
        return $viewModel;
    }

}
