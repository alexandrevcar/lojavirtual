<?php

namespace Application\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;

class Product extends AbstractActionController
{

    public function indexAction()
    {
        $viewModel = new ViewModel();
        $produtoTable = $this->getServiceLocator()->get('Application\Table\Produto');
        $produto = $produtoTable->findById($this->params()->fromRoute('id'));
        $viewModel->setVariable('produto', $produto);
        return $viewModel;
    }

}
