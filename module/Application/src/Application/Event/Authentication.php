<?php

namespace Application\Event;

use Zend\EventManager\ListenerAggregateInterface;
use Zend\EventManager\EventManagerInterface;
use Zend\Authentication\AuthenticationService;
use Zend\Mvc\MvcEvent;
use Zend\Mvc\Router\RouteMatch;
use Zend\Http\Response;

/**
 * @Event
 */
class Authentication implements ListenerAggregateInterface
{

    protected $listeners = [];

    /**
     * @var \Zend\Authentication\AuthenticationService
     */
    protected $authenticationService;

    /**
     * @var type string
     */
    protected $unauthorizedRoute;

    /**
     * @var array
     */
    protected $unauthorizedRouteParams = [];

    /**
     * @var string
     */
    protected $authenticationParameter = 'authentication';

    /**
     * @var string
     */
    protected $redirectParameter = 'redirect';

    public function attach(EventManagerInterface $events)
    {
        $this->listeners[] = $events->attach(MvcEvent::EVENT_ROUTE, [$this, 'checkAuthetication']);
    }

    public function detach(EventManagerInterface $events)
    {
        foreach ($this->listeners as $index => $listener) {
            if ($events->detach($listener)) {
                unset($this->listeners[$index]);
            }
        }
    }

    public function getAuthenticationService()
    {
        return $this->authenticationService;
    }

    public function getUnauthorizedRoute()
    {
        return $this->unauthorizedRoute;
    }

    public function getUnauthorizedRouteParams()
    {
        return $this->unauthorizedRouteParams;
    }

    public function getAuthenticationParameter()
    {
        return $this->authenticationParameter;
    }

    public function getRedirectParameter()
    {
        return $this->redirectParameter;
    }

    public function setAuthenticationService(AuthenticationService $authenticationService)
    {
        $this->authenticationService = $authenticationService;
        return $this;
    }

    public function setUnauthorizedRoute($route)
    {
        $this->unauthorizedRoute = $route;
        return $this;
    }

    public function setUnauthorizedRouteParams(array $unauthorizedRouteParams)
    {
        $this->unauthorizedRouteParams = $unauthorizedRouteParams;
        return $this;
    }

    public function setAuthenticationParameter($authenticationParameter)
    {
        $this->authenticationParameter = (string) $authenticationParameter;
        return $this;
    }

    public function setRedirectParameter($redirectParameter)
    {
        $this->redirectParameter = $redirectParameter;
        return $this;
    }

    public function checkAuthetication(MvcEvent $event)
    {
        $response = $event->getResponse() ?: new Response;
        $routeMatch = $event->getRouteMatch();

        if ((!$routeMatch || !$routeMatch instanceof RouteMatch) || (!$routeMatch->getParam($this->getAuthenticationParameter(), false)) || ($this->getAuthenticationService()->hasIdentity()) || ($routeMatch->getMatchedRouteName() == $this->getUnauthorizedRoute())
        ) {
            return true;
        }

        //unauthorized
        $response->setStatusCode(Response::STATUS_CODE_401);

        if ($this->getUnauthorizedRoute()) {
            $params = $this->getUnauthorizedRouteParams();
            $options = ['name' => $this->getUnauthorizedRoute(), 'query' => []];
            if ($this->getRedirectParameter()) {
                $options['query'][$this->getRedirectParameter()] = $event->getRequest()->getRequestUri();
            }
            $url = $event->getRouter()->assemble($params, $options);

            $response->setStatusCode(Response::STATUS_CODE_302);
            $response->getHeaders()->addHeaderLine('Location', $url);
        }

        return $response;
    }

}
