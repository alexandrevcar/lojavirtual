<?php

namespace Application\Service;

use Zend\ServiceManager\FactoryInterface;
use Zend\ServiceManager\ServiceLocatorInterface;
use Zend\Authentication\Adapter\DbTable as DbTableAdapter;
use Zend\Session\SessionManager;
use Zend\Authentication\Storage\Session as SessionStorage;
use Zend\Authentication\AuthenticationService;

class AuthenticationFactory implements FactoryInterface
{

    /**
     * {@inheritDoc}
     * @return \Zend\Authentication\AuthenticationService
     */
    public function createService(ServiceLocatorInterface $serviceLocator)
    {
        $authAdapter = new \Application\Service\Authentication\Adapter\Adapter($serviceLocator->get('dbAdapterMaster'));
        $authAdapter->setServiceLocator($serviceLocator);
        $authAdapter->setTableName('loja_clientes')
            ->setIdentityColumn('cli_email')
            ->setCredentialColumn('cli_senha');

        $sessionManager = new SessionManager;
        $authStorage = new SessionStorage('LojaVirtual_Auth', null, $sessionManager);

        return new AuthenticationService($authStorage, $authAdapter);
    }

}
