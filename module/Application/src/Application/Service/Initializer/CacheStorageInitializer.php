<?php

namespace Application\Service\Initializer;

use Zend\ServiceManager\InitializerInterface;
use Zend\ServiceManager\ServiceLocatorInterface;

/**
 * @Initializer
 * @author Eduardo Schmidt <eduschmidt10@gmail.com>
 */
class CacheStorageInitializer implements InitializerInterface
{

    /**
     * {@inheritDoc}
     */
    public function initialize($instance, ServiceLocatorInterface $serviceLocator)
    {
        if (method_exists($instance, 'setCacheStorage')) {
            $cacheStorage = $serviceLocator->get('CacheStorage');
            $instance->setCacheStorage($cacheStorage);
        }
    }

}
