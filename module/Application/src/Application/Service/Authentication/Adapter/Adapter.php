<?php

namespace Application\Service\Authentication\Adapter;

use Zend\Authentication\Adapter\DbTable as DbTableAdapter;
use Zend\Authentication\Adapter\AdapterInterface;
use Zend\ServiceManager\ServiceLocatorAwareTrait;
use Zend\Authentication\Result as AuthenticationResult;

/**
 * @Adapter
 */
class Adapter extends DbTableAdapter implements AdapterInterface
{

    use ServiceLocatorAwareTrait;

    /**
     * {@inheritDoc}
     */
    public function authenticate()
    {
        $authResult = parent::authenticate();
        if ($authResult->isValid()) {
            $clienteTable = $this->getServiceLocator()->get('Application\Table\Cliente');
            $clienteEntity = $clienteTable->findById($this->getResultRowObject()->cli_id);
            $authResult = new AuthenticationResult(AuthenticationResult::SUCCESS, $clienteEntity);
        }

        return $authResult;
    }

}