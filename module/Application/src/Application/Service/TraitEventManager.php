<?php

namespace Application\Service;

use Zend\EventManager\EventManagerInterface;

trait TraitEventManager
{

    /**
     * @var \Zend\EventManager\EventManagerInterface
     */
    protected $eventManager;

    /**
     * @return \Zend\EventManager\EventManagerInterface
     */
    public function getEventManager()
    {
        return $this->eventManager;
    }

    /**
     * @param \Zend\EventManager\EventManagerInterface $eventManager
     * @return \BackOffice\Service\User
     */
    public function setEventManager(EventManagerInterface $eventManager)
    {
        $this->eventManager = $eventManager;
        return $this;
    }

}
