<?php

namespace Application\View\Helper;

use Zend\View\Helper\AbstractHelper;
use Zend\ServiceManager\ServiceLocatorAwareInterface;
use Application\Service\TraitServiceLocator;

class Messages extends AbstractHelper implements ServiceLocatorAwareInterface
{

    use TraitServiceLocator;

    public function __invoke()
    {
        $messagesPlugin = $this->getServiceLocator()
            ->getServiceLocator()
            ->get('ControllerPluginManager')
            ->get('Messages');

        $params = array('messages' => $messagesPlugin);
        return $this->getView()->render('helper/messages', $params);
    }

}
