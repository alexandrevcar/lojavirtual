<?php

namespace Application\View\Helper;

use Zend\View\Helper\AbstractHelper;
use Zend\ServiceManager\ServiceLocatorAwareInterface;
use Application\Service\TraitServiceLocator;

/**
 * Converte os erros do InputFilter para marcadores do bootstrapValidator
 */
class InputFilterToBootstrapValidator extends AbstractHelper implements ServiceLocatorAwareInterface
{

    use TraitServiceLocator;

    /**
     * @param string $formSelector jQuery selector
     * @param \Zend\InputFilter\InputFilter $inputFilter
     * @return \Application\Helper\InputFilterToBootstrapValidator
     */
    public function __invoke($formSelector, array $inputFilter)
    {
        $errorList = array();
        $scriptTemplate = "$(function() { $('%s').data('bootstrapValidator')%s; });";
        $fieldTemplate = ".updateStatus('%s', 'INVALID', '%s')";

        foreach ($inputFilter as $field => $fieldErrors) {
            foreach ($fieldErrors as $validator => $message) {
                $errorList[] = sprintf($fieldTemplate, $field, $validator);
            }
        }

        $scriptLine = sprintf($scriptTemplate, $formSelector, implode('', $errorList));
        $this->getView()->InlineScript()->appendScript($scriptLine);

        return $this;
    }

}
