<?php

namespace Application\View\Helper;

use Zend\View\Helper\AbstractHelper;
use Zend\ServiceManager\ServiceLocatorAwareInterface;
use Application\Service\TraitServiceLocator;

class Categoria extends AbstractHelper implements ServiceLocatorAwareInterface
{

    use TraitServiceLocator;

    public function __invoke()
    {
        $categoriaTable = $this->getServiceLocator()->getServiceLocator()->get('Application\Table\Categoria');
        return $categoriaTable->fetchAllPaginated();
    }

}
