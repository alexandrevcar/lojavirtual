<?php

namespace Application\View\Helper;

use Zend\View\Helper\AbstractHelper;
use Zend\ServiceManager\ServiceLocatorAwareInterface;
use Application\Service\TraitServiceLocator;

class Cliente extends AbstractHelper implements ServiceLocatorAwareInterface
{

    use TraitServiceLocator;

    public function __invoke()
    {
        $authService = $this->getServiceLocator()->getServiceLocator()->get('AuthService');
		return $authService;
    }

}
