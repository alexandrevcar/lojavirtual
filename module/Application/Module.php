<?php

namespace Application;

use Zend\ModuleManager\Feature\AutoloaderProviderInterface;
use Zend\ModuleManager\Feature\ConfigProviderInterface;
use Zend\ModuleManager\Feature\BootstrapListenerInterface;
use Zend\EventManager\EventInterface;
use Zend\Session\SessionManager;
use Zend\Session\Config\SessionConfig;
use Zend\Session\Container;

class Module implements AutoloaderProviderInterface, ConfigProviderInterface, BootstrapListenerInterface
{

    public function getAutoloaderConfig()
    {
        return array(
            'Zend\Loader\StandardAutoloader' => array(
                'namespaces' => array(
                    __NAMESPACE__ => __DIR__ . '/src/' . __NAMESPACE__,
                ),
            ),
        );
    }

    public function getConfig()
    {
        return array_merge_recursive(
            include __DIR__ . '/config/module.config.php', include __DIR__ . '/config/module.routes.php'
        );
    }

    public function onBootstrap(EventInterface $event)
    {
        $application = $event->getTarget();
        $serviceManager = $application->getServiceManager();
        $eventManager = $application->getEventManager();

        date_default_timezone_set('America/Sao_Paulo');

        //Traduzir mensagens de validação
        \Zend\Validator\AbstractValidator::setDefaultTranslator($application->getServiceManager()->get('mvctranslator'));

        $this->initSession(array(
            'remember_me_seconds' => 180,
            'use_cookies' => true,
            'cookie_httponly' => true,
        ));

        //Authentication
        $authListener = new Event\Authentication;
        $authListener->setAuthenticationService($serviceManager->get('AuthService'))
            ->setUnauthorizedRoute('site/login')
            ->setAuthenticationParameter('auth')
            ->setRedirectParameter('redirect');
        $eventManager->attachAggregate($authListener);
    }

    public function initSession($config)
    {
        $sessionConfig = new SessionConfig();
        $sessionConfig->setOptions($config);
        $sessionManager = new SessionManager($sessionConfig);
        $sessionManager->start();
        Container::setDefaultManager($sessionManager);
    }

}
