<?php

return [
    'router' => [
        'routes' => [
            'site' => [
                'type' => 'Literal',
                'options' => [
                    'route' => '/',
                    'defaults' => [
                        'controller' => 'Application\Controller\Main',
                        'action' => 'home',
                        'auth' => false,
                    ],
                ],
                'may_terminate' => true,
                'child_routes' => [
                    'search' => [
                        'type' => 'Literal',
                        'options' => [
                            'route' => 'busca',
                            'defaults' => [
                                'controller' => 'Application\Controller\Main',
                                'action' => 'search',
                                'auth' => false,
                            ],
                        ],
                    ],
                    'category' => [
                        'type' => 'Segment',
                        'options' => [
                            'route' => 'categoria/:id',
                            'constraints' => [
                                'slug' => '[A-Za-z0-9\-]+',
                            ],
                            'defaults' => [
                                'controller' => 'Application\Controller\Main',
                                'action' => 'category',
                                'auth' => false,
                            ],
                        ],
                    ],
                    'products' => [
                        'type' => 'Segment',
                        'options' => [
                            'route' => 'produto/:id',
                            'constraints' => [
                                'id' => '[A-Za-z0-9\-]+',
                            ],
                            'defaults' => [
                                'controller' => 'Application\Controller\Product',
                                'action' => 'index',
                                'auth' => false,
                            ],
                        ],
                    ],
                    'login' => [
                        'type' => 'Literal',
                        'options' => [
                            'route' => 'login',
                            'defaults' => [
                                'controller' => 'Application\Controller\Login',
                                'action' => 'index',
                                'auth' => false,
                            ],
                        ],
                    ],
                    'logout' => [
                        'type' => 'Literal',
                        'options' => [
                            'route' => 'sair',
                            'defaults' => [
                                'controller' => 'Application\Controller\Login',
                                'action' => 'logout',
                                'auth' => false,
                            ],
                        ],
                    ],
                    'shopping-cart' => [
                        'type' => 'Literal',
                        'options' => [
                            'route' => 'carrinho',
                            'defaults' => [
                                'controller' => 'Application\Controller\ShoppingCart',
                                'action' => 'index',
                                'auth' => false,
                            ],
                        ],
                        'may_terminate' => true,
                        'child_routes' => [
                            'login' => [
                                'type' => 'Literal',
                                'options' => [
                                    'route' => '/login',
                                    'defaults' => [
                                        'controller' => 'Application\Controller\ShoppingCart',
                                        'action' => 'login',
                                        'auth' => false,
                                    ],
                                ],
                            ],
                            'confirm' => [
                                'type' => 'Literal',
                                'options' => [
                                    'route' => '/confirmacao',
                                    'defaults' => [
                                        'controller' => 'Application\Controller\ShoppingCart',
                                        'action' => 'confirm',
                                        'auth' => true,
                                    ],
                                ],
                            ],
                            'orders' => [
                                'type' => 'Literal',
                                'options' => [
                                    'route' => '/pedidos',
                                    'defaults' => [
                                        'controller' => 'Application\Controller\ShoppingCart',
                                        'action' => 'orders',
                                        'auth' => true,
                                    ],
                                ],
                                'may_terminate' => true,
                                'child_routes' => [
                                    'detail' => [
                                        'type' => 'Segment',
                                        'options' => [
                                            'route' => '/detalhe/:pedido',
                                            'constraints' => [
                                                'id' => '[A-Za-z0-9\-]+',
                                            ],
                                            'defaults' => [
                                                'controller' => 'Application\Controller\ShoppingCart',
                                                'action' => 'orders-detail',
                                                'auth' => true,
                                            ],
                                        ],
                                    ],
                                ],
                            ],
                            'add' => [
                                'type' => 'Segment',
                                'options' => [
                                    'route' => '/adicionar/:id',
                                    'constraints' => [
                                        'id' => '[A-Za-z0-9\-]+',
                                    ],
                                    'defaults' => [
                                        'controller' => 'Application\Controller\ShoppingCart',
                                        'action' => 'add',
                                        'auth' => false,
                                    ],
                                ],
                            ],
                            'update' => [
                                'type' => 'Literal',
                                'options' => [
                                    'route' => '/atualizar',
                                    'defaults' => [
                                        'controller' => 'Application\Controller\ShoppingCart',
                                        'action' => 'update',
                                        'auth' => false,
                                    ],
                                ],
                            ],
                            'remove' => [
                                'type' => 'Segment',
                                'options' => [
                                    'route' => '/remover/:id',
                                    'constraints' => [
                                        'id' => '[A-Za-z0-9\-]+',
                                    ],
                                    'defaults' => [
                                        'controller' => 'Application\Controller\ShoppingCart',
                                        'action' => 'remove',
                                        'auth' => false,
                                    ],
                                ],
                            ],
                        ],
                    ],
                ],
            ],
        ],
    ],
];
