<?php

return [
    'service_manager' => [
        'factories' => [
            'dbAdapterMaster' => 'Application\Service\DbAdapterMasterFactory',
            'Application\Table\Cep' => 'Application\Table\CepFactory',
            'AuthService' => 'Application\Service\AuthenticationFactory',
            'Application\Table\Produto' => 'Application\Table\ProdutoFactory',
            'Application\Table\Categoria' => 'Application\Table\CategoriaFactory',
            'Application\Table\Cliente' => 'Application\Table\ClienteFactory',
            'Application\Table\Pedido' => 'Application\Table\PedidoFactory',
            'Application\Table\Pedido\Item' => 'Application\Table\Pedido\ItemFactory',
        ],
        'abstract_factories' => [
            'Zend\Cache\Service\StorageCacheAbstractServiceFactory',
        ],
        'initializers' => [
            'Application\Service\Initializer\CacheStorageInitializer',
        ],
    ],
    'controllers' => [
        'invokables' => [
            'Application\Controller\Main' => 'Application\Controller\Main',
            'Application\Controller\Login' => 'Application\Controller\Login',
            'Application\Controller\Product' => 'Application\Controller\Product',
            'Application\Controller\ShoppingCart' => 'Application\Controller\ShoppingCart',
            'Application\Controller\Utils' => 'Application\Controller\Utils',
        ],
    ],
    'controller_plugins' => [
        'invokables' => [
            'Messages' => 'Application\Controller\Plugin\Messages',
        ],
    ],
    'view_manager' => [
        'doctype' => 'HTML5',
        'display_exceptions' => true,
        'display_not_found_reason' => true,
        'exception_template' => 'error/500',
        'not_found_template' => 'error/404',
        'template_map' => [
            'error/404' => __DIR__ . '/../view/error/404.phtml',
            'error/500' => __DIR__ . '/../view/error/500.phtml',
            'layout/clean' => __DIR__ . '/../view/layout/layout.phtml',
            'layout/clean' => __DIR__ . '/../view/layout/clean.phtml',
        ],
        'template_path_stack' => [
            __DIR__ . '/../view',
        ],
        'strategies' => [
            'ViewJsonStrategy',
        ],
    ],
    'view_helpers' => [
        'invokables' => [
            'Messages' => 'Application\View\Helper\Messages',
            'Cliente' => 'Application\View\Helper\Cliente',
            'Categoria' => 'Application\View\Helper\Categoria',
            'InputFilterToBootstrapValidator' => 'Application\View\Helper\InputFilterToBootstrapValidator',
        ],
    ],
    'translator' => [
        'locale' => 'pt_BR',
        'translation_files' => [
                [
                'type' => 'phpArray',
                'filename' => './vendor/zendframework/zend-i18n-resources/languages/pt_BR/Zend_Validate.php'
            ],
        ],
    ],
];
