# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* Quick summary
* Version 1.0
* (https://alexandrevcar@bitbucket.org/alexandrevcar/lojavirtual.git)

### How do I get set up? ###

- Configuration

Instalar o composer (https://getcomposer.org/download)
Excutar o comando na dentro da pasta: php composer install 

Instalar o Node.js (https://nodejs.org/en/download/)
Excutar o comando para instalar o bower: npm install -g bower

Exceutar o comando dentro da pasta: bower install

Configurar o ZF2
Copiar o arquivo /config/autoload/local.php.dist e renomear para local.php
Abrir o arquivo e colocar as configurações de banco de dados

Criar a pasta cache com permissão 775 dentro da: /data

- Database configuration

Importar no banco o arquivo "lojavirtual.sql" na raiz


