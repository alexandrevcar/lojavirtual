/*
SQLyog Professional v12.08 (64 bit)
MySQL - 10.1.13-MariaDB : Database - lojavirtual
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
USE `lojavirtual`;

/*Table structure for table `loja_categorias` */

DROP TABLE IF EXISTS `loja_categorias`;

CREATE TABLE `loja_categorias` (
  `cat_id` int(11) NOT NULL AUTO_INCREMENT,
  `cat_nome` varchar(150) NOT NULL,
  PRIMARY KEY (`cat_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

/*Data for the table `loja_categorias` */

insert  into `loja_categorias`(`cat_id`,`cat_nome`) values (1,'Categoria 1'),(2,'Categoria 2');

/*Table structure for table `loja_clientes` */

DROP TABLE IF EXISTS `loja_clientes`;

CREATE TABLE `loja_clientes` (
  `cli_id` int(11) NOT NULL AUTO_INCREMENT,
  `cli_nome` varchar(150) NOT NULL,
  `cli_email` varchar(150) NOT NULL,
  `cli_senha` varchar(50) NOT NULL,
  PRIMARY KEY (`cli_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

/*Data for the table `loja_clientes` */

insert  into `loja_clientes`(`cli_id`,`cli_nome`,`cli_email`,`cli_senha`) values (1,'Alexandre ','alexandrevieiradecarvalho@gmail.com','123456');

/*Table structure for table `loja_pedidos` */

DROP TABLE IF EXISTS `loja_pedidos`;

CREATE TABLE `loja_pedidos` (
  `ped_id` int(11) NOT NULL AUTO_INCREMENT,
  `ped_cliente_id` int(11) NOT NULL,
  `ped_data` datetime NOT NULL,
  `ped_preco_total` float NOT NULL,
  PRIMARY KEY (`ped_id`),
  KEY `FK_ped_cliente_id` (`ped_cliente_id`),
  CONSTRAINT `FK_ped_cliente_id` FOREIGN KEY (`ped_cliente_id`) REFERENCES `loja_clientes` (`cli_id`)
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=latin1;

/*Data for the table `loja_pedidos` */

insert  into `loja_pedidos`(`ped_id`,`ped_cliente_id`,`ped_data`,`ped_preco_total`) values (9,1,'2018-04-18 13:02:30',75),(10,1,'2018-04-18 13:24:05',75),(11,1,'2018-04-18 14:11:47',75),(12,1,'2018-04-18 14:19:00',75),(13,1,'2018-04-18 14:21:38',96.6),(14,1,'2018-04-18 14:23:09',901.2),(15,1,'2018-04-18 14:38:38',96.6);

/*Table structure for table `loja_pedidos_itens` */

DROP TABLE IF EXISTS `loja_pedidos_itens`;

CREATE TABLE `loja_pedidos_itens` (
  `ped_item_id` int(11) NOT NULL AUTO_INCREMENT,
  `ped_item_produto_id` int(11) NOT NULL,
  `ped_item_pedido_id` int(11) NOT NULL,
  `ped_item_preco_total` float NOT NULL,
  `ped_item_quantidade` int(11) NOT NULL,
  PRIMARY KEY (`ped_item_id`),
  KEY `FK_ped_item_produto_id` (`ped_item_produto_id`),
  KEY `FK_ped_item_pedido_id` (`ped_item_pedido_id`),
  CONSTRAINT `FK_ped_item_pedido_id` FOREIGN KEY (`ped_item_pedido_id`) REFERENCES `loja_pedidos` (`ped_id`) ON DELETE CASCADE,
  CONSTRAINT `FK_ped_item_produto_id` FOREIGN KEY (`ped_item_produto_id`) REFERENCES `loja_produtos` (`prod_id`)
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=latin1;

/*Data for the table `loja_pedidos_itens` */

insert  into `loja_pedidos_itens`(`ped_item_id`,`ped_item_produto_id`,`ped_item_pedido_id`,`ped_item_preco_total`,`ped_item_quantidade`) values (8,1,9,75,1),(9,1,10,75,1),(10,1,11,75,1),(11,1,12,75,1),(12,2,13,96.6,1),(13,1,14,225,3),(14,2,14,676.2,7),(15,2,15,96.6,1);

/*Table structure for table `loja_produtos` */

DROP TABLE IF EXISTS `loja_produtos`;

CREATE TABLE `loja_produtos` (
  `prod_id` int(11) NOT NULL AUTO_INCREMENT,
  `prod_nome` varchar(150) NOT NULL,
  `prod_descricao` text NOT NULL,
  `prod_preco` float NOT NULL,
  `prod_imagem` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`prod_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

/*Data for the table `loja_produtos` */

insert  into `loja_produtos`(`prod_id`,`prod_nome`,`prod_descricao`,`prod_preco`,`prod_imagem`) values (1,'Nome do Produto 1','Descrição do produto	',75,'http://placehold.it/400x250/000/fff'),(2,'Nome do Produto 2','Descrição do Produto',96.6,'http://placehold.it/400x250/000/fff');

/*Table structure for table `loja_produtos_categorias` */

DROP TABLE IF EXISTS `loja_produtos_categorias`;

CREATE TABLE `loja_produtos_categorias` (
  `prod_cat_produto_id` int(11) NOT NULL,
  `prod_cat_categoria_id` int(11) NOT NULL,
  PRIMARY KEY (`prod_cat_produto_id`,`prod_cat_categoria_id`),
  KEY `FK_prod_cat_categoria_id` (`prod_cat_categoria_id`),
  CONSTRAINT `FK_prod_cat_categoria_id` FOREIGN KEY (`prod_cat_categoria_id`) REFERENCES `loja_categorias` (`cat_id`) ON DELETE CASCADE,
  CONSTRAINT `FK_prod_cat_produto_id` FOREIGN KEY (`prod_cat_categoria_id`) REFERENCES `loja_produtos` (`prod_id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `loja_produtos_categorias` */

insert  into `loja_produtos_categorias`(`prod_cat_produto_id`,`prod_cat_categoria_id`) values (1,1),(2,2);

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
